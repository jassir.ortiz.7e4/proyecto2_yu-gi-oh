package com.example.yu_gi_oh

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.yu_gi_oh.databinding.ActivityMainBinding

/*URL API: https://db.ygoprodeck.com/api/v7/cardinfo.php*/
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}